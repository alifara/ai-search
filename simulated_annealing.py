#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.animation as animation
import sympy
import logging

FPS = 120

fig = plt.figure()
ax = plt.axes()

x_space = (0.5, 2.5)
y_space = (-1, 6)


def genF():
    from sympy.abc import x
    from sympy import pi, sin, lambdify, diff

    f = sin(10 * pi * x) / (2 * x) + (x - 1) ** 4
    df = diff(f)

    logging.info(f'F(x) = {f}')
    logging.info(f'dF(x)/dx = {df}')

    ax.set_title(f'F(x) = {f}')

    toLambda = lambda f: lambdify(x, f, 'numpy')
    return toLambda(f), toLambda(df)


def plotInit():
    plt.xlim(x_space)
    plt.ylim(y_space)

def plotF(f, x, x_space):
    plt.xlabel('x')
    plt.ylabel('F(x)')
    ax.plot(x, f(x))

def plotLegend(minima_color, neighbour_color):
    minima_patch = mpatches.Patch(color=minima_color, label='SA Minima')
    neighbour_patch = mpatches.Patch(color=neighbour_color, label='Neighbour')
    plt.legend(handles=[minima_patch, neighbour_patch], loc='upper left')

def plotDelta(Delta):
    fig, ax = plt.subplots()
    ax.set_title('Delta at each iter')
    plt.xlabel('Iteration')
    plt.ylabel('Delta: minima - neighbour')
    x = np.arange(Delta.size)
    ax.plot(x, np.abs(Delta), marker='.', markersize=0.1, color='orange')


def simulated_annealing(f, x_space, x_distribution, schedule, find_neighbour, max_iter, min_energy=np.finfo(np.float).eps):
    Minima = np.empty(max_iter)
    Neighbour = np.empty(max_iter)
    Delta = np.empty(max_iter)

    # HACK: hackty hack hack! fix TODO-1
    Minima[0] = x_distribution(low=x_space[0], high=x_space[1])
    Neighbour[0] = x_distribution(low=x_space[0], high=x_space[1])
    Delta[0] = f(Neighbour[0]) - f(Minima[0])

    t = 1  # iteration
    current = Minima[0]
    while True:
        energy = schedule(t)
        if energy - min_energy < 0 or max_iter <= t:
            Delta[0] = Delta[1]  # HACK
            return Minima[:t + 1], Neighbour[:t + 1], Delta[:t + 1]
        nxt = find_neighbour(energy, current)
        delta = f(nxt) - f(current)
        if delta < 0:
            current = nxt
        else:
            prob = min(-delta / energy, 1)
            if np.random.binomial(1, np.float_power(np.e, prob)):
                current = nxt

        Minima[t] = current
        Neighbour[t] = nxt
        Delta[t] = delta
        t += 1


def update(t, f, schedule, Minima, Neighbour, minima_point, neigh_point, iter_text, temp_text):
    '''
    Draws each frame of the simulation.
    '''
    neigh_point.set_data(Neighbour[t], f(Neighbour[t]))
    minima_point.set_data(Minima[t], f(Minima[t]))
    iter_text.set_text(f'iter: {t + 1}')
    temp_text.set_text(f'energy: {round(schedule(t + 1), 4)}')
    return neigh_point, minima_point, iter_text, temp_text


def main():
    logging.getLogger().setLevel(logging.INFO)
    logging.info('Start')

    f, df = genF()

    bound = lambda low, high, y: min(high, max(low, y))

    ### Annealing Schedule
    inverse_square = lambda t: (1/(np.float_power(t, 0.5) + 1))
    bounded_inverse = lambda t: bound(0, 1, inverse_square(t))
    sigmoid_function = lambda t: (- (1 / (1 + np.float_power(np.e, -(t) / 100))) + 1) * 100
    bounded_sigmoid = lambda t: bound(0, 1, sigmoid_function(t))
    shifted_bounded_sigmoid = lambda t: bounded_sigmoid(t + 450)

    ### Neighbourhood generator
    gradient_cointoss_neighbour = lambda e, x, df, alpha, distribution: \
                            x + (alpha * df(x)) if distribution(.5) else x - (alpha * df(x))
    interval_neighbour = lambda e, minima, x_space, x_distribution:\
                            x_distribution(low=max(x_space[0], minima - 2 * e),
                                          high=min(x_space[1], minima + 2 * e))
    gradient_inverval_neighbour  = lambda e, x, x_space, x_distribution, alpha,:\
                            x_distribution(low=max(x_space[0], bound(*x_space, x + (2 * e * df(x)))),
                                          high=min(x_space[1], bound(*x_space, x - (2 * e * df(x)))))

    # sa_find_neighbour = lambda e, minima: gradient_neighbour(e, minima, df, 0.001, lambda e: np.random.binomial(1,e))

    ### SA Adaptors:
    ## use these two together
    annealing_schedule = shifted_bounded_sigmoid
    sa_find_neighbour = lambda e, minima: gradient_inverval_neighbour(e, minima, x_space, x_distribution, 0.1)
    ## or these two together
    # annealing_schedule = bounded_inverse
    # sa_find_neighbour = lambda e, minima: interval_neighbour(e, minima, x_space, x_distribution)

    x_distribution = np.random.uniform  # TODO-1: remove useless distribution
    Minima, Neighbour, Delta = simulated_annealing(f=f,
                                                   x_space=x_space,
                                                   x_distribution=x_distribution,
                                                   schedule=annealing_schedule,
                                                   find_neighbour=sa_find_neighbour,
                                                   max_iter=10_000,
                                                   min_energy=0.001)
    logging.info(f"SA iterations: {len(Minima) - 1}")

    ### Plotting stuff
    plotInit()
    plotF(f, np.linspace(*x_space, 1000), x_space)
    colors = ('crimson', 'lime')
    plotLegend(*colors)

    neigh_point,  = ax.plot([Neighbour[0]], [f(Neighbour[0])], marker='o', markersize=3, color=colors[1])
    minima_point, = ax.plot([Minima[0]], [f(Minima[0])], marker='o', markersize=3, color=colors[0])
    iter_text     = ax.text(0.6, 4, "iter: 0", size=12)
    temp_text     = ax.text(0.6, 3.5, f"energy: {annealing_schedule(0)}", size=12)

    fargs = [f, annealing_schedule, Minima, Neighbour, minima_point, neigh_point, iter_text, temp_text]
    ani = animation.FuncAnimation(fig, update, len(Minima) - 1, fargs=fargs,
                                interval=1000 / FPS, blit=True, repeat=False)

    # ani.save('ani_sa.gif', writer='imagemagick')
    # ani.save('ani_sa.gif')
    plt.show()
    # fig.savefig('ani_sa.png')

    plotDelta(Delta)
    plt.show()

if __name__ == "__main__":
    main()
