#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.animation as animation
import logging

FPS = 3

fig = plt.figure()
ax = plt.axes()

x_space = (0.5, 2.5)
y_space = (-1, 6)

def genF():
    from sympy.abc import x
    from sympy import pi, sin, lambdify, diff

    f = sin(10 * pi * x) / (2 * x) + (x - 1) ** 4
    df = diff(f)

    logging.info(f'F(x) = {f}')
    logging.info(f'dF(x)/dx = {df}')

    ax.set_title(f'f(x) = {f}')

    toLambda = lambda f: lambdify(x, f, 'numpy')
    return toLambda(f), toLambda(df)

def plotInit():
    plt.xlim(x_space)
    plt.ylim(y_space)

def plotF(f, x, x_space):
    ax.plot(x, f(x))

def plotLegend(start_color, minima_color, all_mimina):
    start_patch = mpatches.Patch(color=start_color, label='Start Point')
    minima_patch = mpatches.Patch(color=minima_color, label='Found Min')
    all_mimina = mpatches.Patch(color=all_mimina, label='Found Minima')
    plt.legend(handles=[start_patch, minima_patch, all_mimina], loc='upper left')

# def plotTheta(Theta, f):
#     fig, ax = plt.subplots()
#     ax.set_title('Step size at each iter')

#     a = Theta[:-1]
#     b = Theta[1:]
#     hTheta = f(a) - f(b)
#     iters = np.arange(np.shape(a)[0])
#     ax.plot(iters, hTheta)

def grad_desc(start_x, df, learning_rate, n_iter):
    assert n_iter > 0

    alpha = learning_rate
    x = start_x
    for i in range(n_iter):
        x -= alpha * df(x)

    logging.info(f'Found minima\'s x: {x}')
    return x

def iter_hill_climbing(df, x_space, x_distribution, n_iters, learning_rate, n_iters_grad_desc):
    assert n_iters > 1

    Theta = np.empty(n_iters)
    minima = np.empty(n_iters)
    start_x = x_distribution(*x_space, n_iters)

    minima[-1] = np.inf
    for i in range(n_iters):  # TODO: remove for loop
        g = grad_desc(start_x=start_x[i], df=df, learning_rate=learning_rate, n_iter=n_iters_grad_desc)
        Theta[i] = g
        minima[i] = min(minima[i - 1], g)

    logging.info(f'Found global minima: f({minima[-1]})')
    return minima, Theta, start_x

def update(t, minima, Theta, start_x , minima_point, min_point, start_point, iter_text):
    start_point.set_data(start_x[t], f(start_x[t]))
    min_point.set_data(Theta[t], f(Theta[t]))
    minima_point.set_data(minima[t], f(minima[t]))
    iter_text.set_text(f'iter: {str(t + 1)}')
    return start_point, min_point, minima_point, iter_text

if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)

    f, df = genF()

    plotInit()
    plotF(f, np.linspace(*x_space, 1000), x_space)
    colors = ('fuchsia', 'crimson', 'lime')
    plotLegend(*colors)

    minima, Theta, start_x = iter_hill_climbing(df=df,
                                                x_space=x_space,
                                                x_distribution=np.random.uniform,
                                                n_iters=20,
                                                learning_rate=0.001,
                                                n_iters_grad_desc=100)

    start_point,  = ax.plot([start_x[0]], [f(start_x[0])], marker='o', markersize=3, color=colors[0])
    min_point,    = ax.plot([Theta[0]], [f(Theta[0])], marker='o', markersize=3, color=colors[1])
    minima_point, = ax.plot([minima[0]], [f(minima[0])], marker='o', markersize=3, color=colors[2])
    iter_text     = ax.text(0.6, 3.5, "iter: 0", size=20)

    fargs = [minima, Theta, start_x , minima_point, min_point, start_point, iter_text]
    ani = animation.FuncAnimation(fig, update, len(minima), fargs=fargs,
                                interval=1000/FPS, blit=True, repeat=False)

    # ani.save('ani.gif', writer='imagemagick')
    # ani.save('ani.gif')
    plt.show()

    # plotTheta(Theta, f)
    # plt.show()
