#!/usr/bin/env python3
from __future__ import annotations  # https://stackoverflow.com/questions/33533148/how-do-i-specify-that-the-return-type-of-a-method-is-the-same-as-the-class-itsel

import numpy as np
import matplotlib.pyplot as plt
# import matplotlib.patches as mpatches
# import matplotlib.animation as animation
# import sympy
import logging

import operator

from functools import reduce
from typing import Set, Tuple, List, Sequence, Optional, Dict


all_genes_list = [i for i in range(1, 11)]
all_genes = set(all_genes_list)

class Chromosome:
    def __init__(self, genes: Set, score=None):
        assert(isinstance(genes, Set))
        self.A: Set = genes
        self._score: Optional[int] = score  # it'll be an int once it has a score

    def __repr__(self):
        return f"A: {self.A}, B:{self.B}, Score:{self._score}"

    def copy(self):
        return Chromosome(self.A.copy(), self._score)

    @property
    def B(self):
        return all_genes - self.A

    @property
    def fitness(self):
        if self._score is None:
            raise Exception('Chromosome does not have fitness value.')
        return self._score

    @fitness.setter
    def fitness(self, value: int):
        # assert(isinstance(value, float))
        self._score = value

    @staticmethod
    def _exchange_at_middle(c1: Set, c2: Set) -> Tuple[Set, Set]:
        assert(len(c1) == len(c2))

        lc1 = list(c1)
        lc2 = list(c2)
        lc1.sort()
        lc2.sort()
        idx = np.random.randint(len(lc1) + 1)

        c1_ = set(lc1[:idx] + lc2[idx:])
        c2_ = set(lc2[:idx] + lc1[idx:])
        return (c1_, c2_)

    @staticmethod
    def crossover(c1: Chromosome, c2: Chromosome) -> Tuple[Chromosome, Chromosome]:
        comm = c1.A.intersection(c2.A)
        distinct_c1 = c1.A - comm
        distinct_c2 = c2.A - comm
        c1_, c2_ = Chromosome._exchange_at_middle(distinct_c1, distinct_c2)
        return (Chromosome(genes=comm.union(c1_)),
                Chromosome(genes=comm.union(c2_)))

    def _swap_between_A_B(self) -> None:
        # IDEA: add a probability for second mutation?
        B = self.B
        tupleA = tuple(self.A)
        tupleB = tuple(B)

        assert(len(tupleA) == 5)
        assert(len(tupleB) == 5)

        new_replacing_gene = np.random.choice(tupleB, 1)[0]  # uniform probability
        to_be_replaced_gene = np.random.choice(tupleA, 1)[0]  # uniform probability

        self.A.remove(to_be_replaced_gene)
        self.A.add(new_replacing_gene)


    def mutate(self) -> None:
        '''
        Mutable method which mutates the Chromosome
        '''
        self._swap_between_A_B()


class Population:
    def __init__(self, chromosomes: List[Chromosome]=None):
        self.individuals = chromosomes if chromosomes else []

    def __repr__(self):
        return str(list(map(lambda x: x.__repr__(), self.individuals)))

    def __len__(self) -> int:
        return len(self.individuals)

    def copy(self):
        return Population(self.individuals.copy())

    def deepcopy(self):
        indivs = list(map(lambda x: x.copy(), self.individuals))
        return Population(indivs)

    def add(self, c: Chromosome) -> None:
        return self.individuals.append(c)

    def _fitness_score(self, c: Chromosome) -> int:
        score_A = (36 - sum(c.A)) ** 2
        score_B = abs(360 - reduce(lambda x,y: x*y, c.B))
        c.fitness = (score_A + score_B)
        return c.fitness

    def _diversity_score(self, c: Chromosome):
        # TODO
        pass

    def fitness(self, c: Chromosome) -> int:
        fits = self._fitness_score(c)
        ## TODO: also take diversity of the population into account
        # dvrs = self._diversity_score(c)
        # return f(fits, dvrs)
        fitness = fits
        return fitness

    def fitnessToProb(self):
        # Ehh
        # TODO
        pass

    def mutateAll(self, should_mutate):
        assert(len(self) == len(should_mutate))
        for c, cond in zip(self.individuals, should_mutate):
            if cond:
                c.mutate()

    def crossoverAll(self, src_population: Population):
        # TODO: Take diversity into account and sample from the population, insted of shutgon spraying everyone
        # IDEA: use MCMC or Gibs sampling
        for i in range(len(src_population)):
            for j in range(i + 1, len(src_population)):
                u, v = src_population.individuals[i], src_population.individuals[j]
                c1, c2 = Chromosome.crossover(u, v)
                self.individuals.append(c1)
                self.individuals.append(c2)

    def sortIndividualsByFitness(self):
        # for c in self.individuals:
        #     c.fitness(self._fitness_score(c))
        self.individuals.sort(key=operator.attrgetter('fitness'))

    def ranked(self):
        return self.individuals


def plotPopulations(plot, pops: Sequence[Population]):
    cmap = plt.get_cmap('rainbow')

    for i, p in enumerate(pops):
        scores = np.ones(len(p), dtype=np.int)
        for idx in range(len(p)):
            scores[idx] = p.individuals[idx].fitness
        x = np.full(len(scores), i + 1, dtype=np.int)
        plt.scatter(x, scores, c=scores, s=10, marker='o', alpha=0.5, cmap=cmap)


### optimization opportunity: use a DS with Ω(1) access time
rank_space_dp: Dict[int, float] = {}
def rank_space(i: int, prune_prob: float):
    global rank_space_dp

    value = rank_space_dp.get(i)
    if value is not None:
        return value

    func = lambda i: prune_prob * (1 - prune_prob) ** i
    assignment_isnt_an_statement_python = rank_space_dp[i] = func(i)
    return assignment_isnt_an_statement_python

def genetic_algorithm(init_population: Population,
                      goal_met,
                      max_population_size: int,
                      prune_prob: float,
                      max_iter: int):
    populations:List[Population] = [init_population]

    ## getting ready
    t = 1  # iteration counter
    p = populations[0]
    for c in p.individuals: # HACK
            p.fitness(c)

    while True:
        if max_iter < t or goal_met(p.individuals[0].fitness):
            # TODO
            c = p.individuals[0]
            logging.info(f'Best first found solution: {c}\n\tSum(A): {sum(c.A)}, Mult(B): {reduce(lambda x,y: x*y, c.B)}')
            return populations

        logging.info(f"Generation {t} start. size:{len(p)}")
        ## mutation
        # optimization opportunity: don't allocate a new np.array everytime
        should_mutate = np.random.binomial(1, 0.4, size=len(p))
        p.mutateAll(should_mutate)

        ## crossover
        p_cross = Population()  # let's not keep the parents
        p_cross.crossoverAll(src_population=p)

        ## Gynotype -> Phenotype
        for c in p_cross.individuals:  # HACK
            p_cross.fitness(c)

        ## fitness
        p_cross.sortIndividualsByFitness()

        ## fitness -> p_i
        # select_using_prob = lambda rank: np.random.binomial(1, rank_space(rank, prune_prob))

        ## selection using p_i
        p_new = Population(p_cross.individuals[:max_population_size])
        # TODO: use MCMC or Gibs sampling
        # for rank, c in zip(range(len(p_cross)), p_cross.ranked()):
        #     if j >= max_population_size:
        #         break
        #     if select_using_prob(rank):
        #         p_new.add(c)
        #         j += 1

        logging.info(f"Generation {t} end. new_size:{len(p_new)}, cross_size:{len(p_cross)}")

        ## save stuff
        populations.append(p_new.deepcopy())

        ## get ready for next iteration
        p = p_new
        t += 1


def main():
    logging.getLogger().setLevel(logging.INFO)
    logging.info('Start')

    init_pop_size = 40
    max_population_size = 50
    prune_prob = 0.0 # TODO
    max_iter = 20
    goal_met = lambda fitness: fitness == 0

    bag = all_genes_list.copy()
    def gen_radom_choro():
        nonlocal bag
        np.random.shuffle(bag)
        return Chromosome(set(bag[0:5]))

    init_indiviuals = [gen_radom_choro() for i in range(init_pop_size)]

    # logging.info(init_indiviuals)

    populations = genetic_algorithm(init_population=Population(init_indiviuals),
                                    goal_met=goal_met,
                                    max_population_size=max_population_size,
                                    prune_prob=prune_prob,
                                    max_iter=max_iter)

    # logging.info(populations)

    plot = plt.subplot()
    plot.set_xlabel('Generation')
    plot.set_ylabel('Fitness')
    plt.xticks(range(1, len(populations) + 1))
    plot.axhline(y=0, color='r', ls='-', lw=1, alpha=0.2)
    plotPopulations(plt, populations)
    plt.show()

if __name__ == "__main__":
    main()