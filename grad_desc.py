#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import logging

fig = plt.figure()
ax = plt.axes()

x_space = (0.5, 2.5)
y_space = (-1, 6)

def genF():
    from sympy.abc import x
    from sympy import pi, sin, lambdify, diff

    f = sin(10 * pi * x) / (2 * x) + (x - 1) ** 4
    df = diff(f)

    logging.info(f'F(x) = {f}')
    logging.info(f'dF(x)/dx = {df}')

    ax.set_title(f'f(x) = {f}')

    toLambda = lambda f: lambdify(x, f, 'numpy')
    return toLambda(f), toLambda(df)

def plotInit():
    global x
    plt.xlim(x_space)
    plt.ylim(y_space)

def plotF(f, x, x_space):
    ax.plot(x, f(x))

def plotPoint(x, y, color='green'):
    ax.plot([x], [y], marker='o', markersize=3, color=color)

def plotLegend(start_color, minima_color):
    start_patch = mpatches.Patch(color=start_color, label='Start Point')
    minima_patch = mpatches.Patch(color=minima_color, label='Found Minima')
    plt.legend(handles=[start_patch, minima_patch], loc='upper left')

def plotTheta(Theta, f):
    fig, ax = plt.subplots()
    ax.set_title('Step size at each iter')

    a = Theta[:-1]
    b = Theta[1:]
    hTheta = f(a) - f(b)
    iters = np.arange(np.shape(a)[0])
    ax.plot(iters, hTheta)


def grad_desc(start_x, df, learning_rate, n_iter):
    assert n_iter > 0

    alpha = learning_rate

    Theta = np.empty(n_iter + 1)
    Theta[0] = start_x
    for i in range(1, n_iter + 1):
        x = Theta[i - 1]
        Theta[i] = x - alpha * df(x)

    logging.info(f'Found minima\'s x: {x}')
    return x, Theta

if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)

    f, df = genF()

    plotInit()
    plotF(f, np.linspace(*x_space, 1000), x_space)

    start_x = np.random.uniform(*x_space)
    logging.info(f"start x: {start_x}",)
    plotPoint(start_x, f(start_x), 'red')

    x, Theta = grad_desc(start_x=start_x, df=df, learning_rate=0.001, n_iter=100)
    plotPoint(x, f(x), 'lime')

    plotLegend(start_color='red', minima_color='lime')
    plt.show()

    plotTheta(Theta, f)
    plt.show()