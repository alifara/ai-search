import numpy as np
import logging
import operator
import pathlib


POPULATION_MAX_SIZE = 500
POPULATION_INIT_SIZE = 500

base_table = None


class Chromosome:
    def __init__(self, table=None, fitness_score=None):
        self.table = table
        self._score = fitness_score

    def __repr__(self) -> str:
        return f"fitness: {self.fitness}, table:\n{self.table}"

    def __str__(self) -> str:
        return f"<class Chromosome, {self.__repr__()}>"

    def copy(self):
        return Chromosome(self.table.copy(), self._score)

    @property
    def fitness(self):
        if self._score is None:
            self._score = self.calc_fitness()
            # raise Exception("Chromosome does not have fitness value.")
        return self._score

    @fitness.setter
    def fitness(self, value: int):
        # raise Exception("Someone set my fitness value!")
        # logging.info(msg="Someone set my fitness value!")
        self._score = value

    def check_subgrid(self, i, j) -> int:
        il, ir = i * 3, (i + 1) * 3
        jl, jr = j * 3, (j + 1) * 3
        s = set(self.table[il:ir, jl:jr].ravel()) - {0}
        return 9 - len(s)

    def check_row(self, i) -> int:
        s = set(self.table[i]) - {0}
        return 9 - len(s)

    def check_column(self, j) -> int:
        s = set(self.table[:, j]) - {0}
        return 9 - len(s)

    def calc_fitness(self) -> int:
        fitness = sum(self.check_row(i) for i in range(0, 8))  # << 1
        fitness += sum(self.check_column(i) for i in range(0, 8)) << 1
        fitness += sum(self.check_subgrid(i // 3, i % 3) for i in range(0, 8)) << 1
        return -fitness


class Population:
    def __init__(self, chromosomes=None):
        self.individuals = chromosomes if chromosomes else []

    def __repr__(self):
        return str(list(map(lambda x: x.__repr__(), self.individuals)))

    def __str__(self) -> str:
        return f"<class Population, {self.__repr__()}>"

    def __len__(self) -> int:
        return len(self.individuals)

    def __delitem__(self, key):
        self.individuals.__delattr__(key)

    def __getitem__(self, key):
        return self.individuals.__getitem__(key)

    def __setitem__(self, key, value):
        self.individuals.__setitem__(key, value)

    def copy(self):
        return self.__class__(self.individuals.copy())

    def deepcopy(self):
        indivs = list(map(lambda x: x.copy(), self.individuals))
        return self.__class__(indivs)

    def add(self, c: Chromosome) -> None:
        return self.individuals.append(c)

    def concat(self, lst) -> None:
        self.individuals += lst

    def sort(self, *args, **kwargs):
        return self.individuals.sort(*args, **kwargs)


def cross_over(c1: Chromosome, c2: Chromosome) -> (Chromosome, Chromosome):
    """take the first half of a c1's table and concat it with rest of c2's table."""
    low, high = 0, 8
    # low, high = 1, 7
    split_idx = np.random.randint(low, high + 1)
    offspring1 = Chromosome(table=np.r_[c1.table[:split_idx], c2.table[split_idx:]])
    offspring2 = Chromosome(table=np.r_[c2.table[:split_idx], c1.table[split_idx:]])
    return offspring1, offspring2


def mutation_shuffle_column_inplace(c: Chromosome) -> None:
    """select a column and shuffle it's values."""
    global base_table

    nums_set = set(range(1, 10))
    low, high = 0, 8
    idx = np.random.randint(low, high + 1)
    base_column = base_table[:, idx]
    values = list(set(nums_set) - set(base_column))
    np.random.shuffle(values)
    j = 0
    for i, bc in zip(range(low, high + 1), base_column):
        if bc == 0:
            c.table[i, idx] = values[j]
            j += 1
    c.fitness = c.calc_fitness()


def mutation_shuffle_subgrid_inplace(c: Chromosome, I: int, J: int) -> None:
    """select a subgrid and shuffle it's values."""
    global base_table

    nums_set = set(range(1, 10))

    il, ir = I * 3, (I + 1) * 3
    jl, jr = J * 3, (J + 1) * 3
    base_subgrid = base_table[il:ir, jl:jr].ravel()
    values = list(set(nums_set) - set(base_subgrid))
    np.random.shuffle(values)

    i_base = 0
    i_values = 0
    for i in range(il, ir):
        for j in range(jl, jr):
            cell = base_subgrid[i_base]
            if cell != 0:
                c.table[i][j] = cell
            else:
                c.table[i][j] = values[i_values]
                i_values += 1
            i_base += 1
    c.fitness = c.calc_fitness()


def mutation_shuffle_column_subgrid_inplace(
    c: Chromosome, p_shuffle_column: float = 0.5
) -> None:
    if np.random.binomial(1, p_shuffle_column):
        mutation_shuffle_column_inplace(c)
    else:
        i, j = np.random.randint(0, 3, size=2)
        mutation_shuffle_subgrid_inplace(c, i, j)


def tournament_selection(pops: Population) -> (Chromosome, Chromosome):
    fit = pops[np.random.randint(0, len(pops))]
    weak = pops[np.random.randint(0, len(pops))]
    if fit.fitness < weak.fitness:
        fit, weak = weak, fit

    selection_rate = 0.85
    if np.random.rand() < selection_rate:
        return fit
    else:
        return weak


def initial_population(
    mask: np.ndarray, size: int = POPULATION_INIT_SIZE
) -> Population:
    possible_values_set = set(range(1, 10))

    def get_guy(mask):
        nonlocal possible_values_set
        guy = mask.copy()
        for i in range(0, 9):
            values = list(possible_values_set - set(mask[i]))
            np.random.shuffle(values)
            k = 0
            for j, dontTouch in zip(range(0, 9), mask[i]):
                if not dontTouch:
                    guy[i, j] = values[k]
                    k += 1
        return guy

    return Population([Chromosome(get_guy(mask)) for i in range(size)])


def genetic_algorithm(
    f_mutate_chromosome=mutation_shuffle_column_subgrid_inplace,
    f_initial_population=initial_population,
    f_cross_over=cross_over,
    mutation_probability=0.15,
    max_population_size=POPULATION_MAX_SIZE,
    max_stale=20,
    max_iter=200,
):
    global base_table

    population = f_initial_population(mask=base_table, size=max_population_size)
    population.sort(key=operator.attrgetter("fitness"))
    stale = 0
    dont_ask = False
    first_c = population[0]
    best_c = population[-1]

    N_elite = int(max_population_size * 0.05)

    for iter_idx in range(max_iter):
        # Show best and worst
        logging.info(
            msg=f"""iter {iter_idx + 1}
best: {population[0]},
worst: {population[-1]}"""
        )
        logging.info(msg="-" * 80)
        # are we done yet?
        if first_c.fitness == 0:
            print(f"found solution!: at iteration{iter_idx + 1}", first_c, sep="\n")
            return

        ## Crossover + Mutation + Selection (using tournament)
        size_diff = max_population_size - N_elite
        logging.info(msg="crossover + mutation start")
        new_chromosomes = [None for i in range(N_elite, max_population_size - 1)] + (
            [None] if 1 - (size_diff % 2) else []
        )
        # print(max_population_size - N_elite)
        for i in range(0, size_diff - (size_diff % 2), 2):
            p1 = tournament_selection(population)
            p2 = tournament_selection(population)
            c1, c2 = f_cross_over(p1, p2)

            should_mutate = np.random.binomial(1, mutation_probability, size=2)
            if should_mutate[0]:
                f_mutate_chromosome(c1)
            if should_mutate[1]:
                f_mutate_chromosome(c2)

            new_chromosomes[i] = c1
            new_chromosomes[i + 1] = c2

        new_population = Population(population.individuals[:N_elite] + new_chromosomes)
        logging.info(msg="crossover + mutation end")

        # for i in new_population.individuals:
        #     print(type(i))

        ## Sort
        logging.info(msg="sorting start")
        new_population.sort(key=operator.attrgetter("fitness"), reverse=True)
        logging.info(msg="sorting end")

        ## New generation
        first_c = new_population[0]
        if best_c.fitness < first_c.fitness:
            best_c = first_c

        if new_chromosomes[0].fitness == population[0].fitness:
            stale += 1

        if stale == max_stale:
            logging.info(msg="Population is stale, regenerating new population.")
            population = f_initial_population(mask=base_table, size=max_population_size)
            population.sort(key=operator.attrgetter("fitness"))
            cbest = new_population[0]
            # print([cbest.check_row(i) == 0 for i in range(9)])
            # print([cbest.check_column(i) == 0 for i in range(9)])
            # print([cbest.check_subgrid(i % 3, i // 3) == 0 for i in range(9)])
            stale = 0
            if not dont_ask:
                opt = input(
                    "Population is stale, regenerating new population."
                    + "Continue? ([y]es/[n]o/[a]ll): "
                ).upper()[0]
                if opt == "N":
                    break
                elif opt == "A":
                    dont_ask = True
        else:
            population = new_population

    print(
        "couldn't find solution :'(",
        f"\nHere's the best I got",
        best_c,
        sep="\n",
    )


def main():
    file_name = ["sudoku/cinch.txt", "sudoku/easy.txt", "sudoku/prob.txt"][-3]
    path = pathlib.Path(__file__).parent.absolute()

    with open(path / file_name, "r") as fp:
        input = list(map(int, fp.readline()[:-1]))
        base = np.array(input, dtype=np.int)
        base.shape = (9, 9)

    global base_table
    base_table = base

    # Comment line below to prevent logging by GA and keeping terminal clean
    logging.getLogger().setLevel(logging.INFO)

    popsize = 500
    genetic_algorithm(
        mutation_probability=0.2,  # not really 0.2, look at mutation function
        max_population_size=popsize,
        max_iter=400,
    )


if __name__ == "__main__":
    main()


# def testMain():
#     file_name = ["sudoku.txt", "sudoku_easy.txt", "sudoku_prob.txt"][-3]

#     with open(file_name, "r") as fp:
#         input = list(map(int, fp.readline()[:-1]))
#         base = np.array(input, dtype=np.int)
#         base.shape = (9, 9)

#     global base_table
#     base_table = base
#     # print(base)

#     logging.getLogger().setLevel(logging.INFO)
#     # initial_population(base)

#     # c = Chromosome(table=base)
#     # print(c.check_subgrid(2, 1))
#     # print(c.check_row(1))
#     # print(c.check_column(0))
#     # print("-" * 80)

#     # # c.fitness = calc_fitness(c)
#     # print(c)
#     # print(c.check_subgrid(0, 0))
#     # print(c.check_subgrid(1, 1))
#     # print("-" * 80)

#     # pops = initial_population(mask=base_table, size=3)
#     # for p in pops.individuals:
#     #     print(p)
#     #     print()
#     # print("-" * 80)

#     # print(cross_over(pops[-1], pops[0]))
#     # print("-" * 80)

#     # c = pops[0]
#     # print("base", base, sep="\n")
#     # print("before", c)
#     # mutation_shuffle_column_inplace(c)
#     # print("after", c)
#     # print("-" * 80)

#     # c = pops[0]
#     # print("base", base, sep="\n")
#     # print("before", c)
#     # mutation_shuffle_subgrid_inplace(c, 0, 0)
#     # print("after", c)
#     # print("-" * 80)

#     # c = Chromosome(
#     #     np.array(
#     #         [
#     #             [5, 4, 3, 9, 2, 1, 6, 9, 7],
#     #             [9, 6, 7, 3, 4, 5, 8, 2, 1],
#     #             [2, 8, 1, 8, 7, 6, 4, 5, 3],
#     #             [4, 5, 8, 1, 3, 2, 9, 7, 6],
#     #             [7, 2, 9, 5, 6, 4, 1, 3, 8],
#     #             [1, 3, 6, 7, 9, 8, 2, 5, 4],
#     #             [3, 7, 2, 6, 8, 9, 5, 8, 1],
#     #             [8, 1, 4, 2, 5, 3, 7, 6, 9],
#     #             [6, 9, 5, 4, 1, 7, 3, 4, 2],
#     #         ]
#     #     )
#     # )
#     # print(c)
#     # print([c.check_row(i) for i in range(9)])
#     # print([c.check_column(i) for i in range(9)])
#     # print([c.check_subgrid(i % 3, i // 3) for i in range(9)])
#     # quit()

#     popsize = 1000
#     f_mutation = lambda c: mutation_shuffle_column_subgrid_inplace(c, 0.5)
#     genetic_algorithm(
#         f_mutate_chromosome=f_mutation,
#         mutation_probability=0.2,
#         max_population_size=popsize,
#         max_iter=400,
#     )
